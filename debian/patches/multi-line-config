From: Ricardo Ribalda <ribalda@chromium.org>
Date: Wed, 25 Dec 2024 22:22:15 +0000
Subject: get_config_settings: Avoid break with multiline configs

If a user has a multiline config like:
```
git config test.test = "Hello
world"
```

git-filter-repo breaks:
```
>     return dict(line.split(b'=', maxsplit=1)
                  for line in output.strip().split(b"\n"))
E     ValueError: dictionary update sequence element #12 has length 1; 2 is required
```

Multiline configs are required by b4 in branch-description
strategy:
https://b4.docs.kernel.org/en/latest/contributor/prep.html#prep-cover-strategies

Signed-off-by: Ricardo Ribalda <ribalda@chromium.org>
Origin: https://github.com/newren/git-filter-repo/pull/624
Bug-Debian: https://bugs.debian.org/1091422
---
 git-filter-repo | 6 ++++--
 1 file changed, 4 insertions(+), 2 deletions(-)

diff --git a/git-filter-repo b/git-filter-repo
index a40bce5..7ff048b 100755
--- a/git-filter-repo
+++ b/git-filter-repo
@@ -1688,9 +1688,11 @@ class GitUtils(object):
     except subprocess.CalledProcessError as e: # pragma: no cover
       raise SystemExit('fatal: {}'.format(e))
 
+    configs = map(lambda x: x.strip().split(b'=', maxsplit=1), output.splitlines())
+    # FIXME: Ignores multi-line values, just take the first line for now
+    configs = filter(lambda x: len(x) == 2, configs)
     # FIXME: Ignores multi-valued keys, just let them overwrite for now
-    return dict(line.split(b'=', maxsplit=1)
-                for line in output.strip().split(b"\n"))
+    return dict(configs)
 
   @staticmethod
   def get_blob_sizes(quiet = False):
